using System;
using System.Linq;
using System.Collections.Generic;

class Point
{
    public int x;
    public int y;

    // The Manhattan Distance
    public int getDistance(Point other){
        return Math.Abs(this.x - other.x) + Math.Abs(this.y - other.y);
    }

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        
        if(obj is Point){
            var p = obj as Point;
            if(this.x == p.x && this.y == p.y)
                return true;
            else
                return false;
        }
        return base.Equals (obj);
    }
    
    // override object.GetHashCode
    public override int GetHashCode()
    {
        return (this.x + "," + this.y).GetHashCode();
    }

    public Point(string init){
        var input = init.Split(" ");
        this.x = int.Parse(input[0]);
        this.y = int.Parse(input[1]);
    }
}
class Day6 : IDay
{
    public override string getInput()
    {
        return "124 262;182 343;79 341;44 244;212 64;42 240;225 195;192 325;192 318;42 235;276 196;181 262;199 151;166 214;49 81;202 239;130 167;166 87;197 53;341 346;235 241;99 278;163 184;85 152;349 334;175 308;147 51;251 93;163 123;151 219;162 107;71 58;249 293;223 119;46 176;214 140;80 156;265 153;92 359;103 186;242 104;272 202;292 93;304 55;115 357;43 182;184 282;352 228;267 147;248 271";
    }

    public override string part1()
    {
        // Input Formatting & Parsing
        var input = getInput().Split(";").Select(z => new Point(z));
        // <Point ID, Point>
        var finput = new Dictionary<int, Point>();
        var id = 0;
        foreach (var p in input)
        {
            finput.Add(id++,p);
        }

        // Determining the Grid to use
        var left = finput.Aggregate((a,z)=>a.Value.x < z.Value.x ? a : z).Value.x;
        var right = finput.Aggregate((a,z)=>a.Value.x > z.Value.y ? a : z).Value.y;
        var up = finput.Aggregate((a,z)=>a.Value.y < z.Value.y ? a : z).Value.y;
        var down = finput.Aggregate((a,z)=>a.Value.y > z.Value.y ? a : z).Value.y;

        // <PointOnGrid,closestPointID>>
        var grid = new Dictionary<Point,int>();

        for (int i = left; i < right; i++)
        {
            for (int j = up; j < down; j++)
            {
                var here = new Point(i,j);
                var cID = -1;
                Point closest;
                var dist = float.MaxValue;
                foreach (var point in finput)
                {
                    var d = here.getDistance(point.Value);
                    if(d == dist){
                        cID = -1;
                    }
                    if (d < dist)
                    {
                        dist = d;
                        closest = point.Value;
                        cID = point.Key;
                    }
                }
                grid.Add(here,cID);
            }
        }

        var invalidIDs = grid.Where(z => z.Key.x == right || z.Key.x == left || z.Key.y == up || z.Key.y == down).Select(z => z.Value);
        var saneGrid = grid.Where(z => !invalidIDs.Contains(z.Value)).ToList();
        var winningID = saneGrid.Select(z => z.Value).GroupBy(z => z).Aggregate((a,z)=>{
            return a.Count() > z.Count() ? a : z;
        }).Count();

        return winningID.ToString();
    }


    public override string part2()
    {
        // Input Formatting & Parsing
        var input = getInput().Split(";").Select(z => new Point(z));
        // <Point ID, Point>
        var finput = new Dictionary<int, Point>();
        var id = 0;
        foreach (var p in input)
        {
            finput.Add(id++,p);
        }

        // Determining the Grid to use
        var left = finput.Aggregate((a,z)=>a.Value.x < z.Value.x ? a : z).Value.x;
        var right = finput.Aggregate((a,z)=>a.Value.x > z.Value.y ? a : z).Value.y;
        var up = finput.Aggregate((a,z)=>a.Value.y < z.Value.y ? a : z).Value.y;
        var down = finput.Aggregate((a,z)=>a.Value.y > z.Value.y ? a : z).Value.y;

        // <PointOnGrid,cumulativeDistanceToPoints>
        var grid = new Dictionary<Point,int>();

        for (int i = left-1000; i < right+1000; i++)
        {
            for (int j = up-1000; j < down+1000; j++)
            {
                var here = new Point(i,j);
                var dist = 0;
                foreach (var point in finput)
                {
                    dist += here.getDistance(point.Value);
                }
                grid.Add(here,dist);
            }
        }

        var result = grid.Where(z => z.Value < 10000).Count();

        return result.ToString();
    }
}